﻿using System;

namespace JM2k.Controller
{
    public interface IResolver
    {
        T Resolve<T>();

        object Resolve(Type type);

        void RegisterInstance<T>(T instance) where T : class;

        void RegisterSingleton<TFrom, TTo>() where TTo : TFrom;

        void Register<TFrom, TTo>() where TTo : TFrom;
    }
}