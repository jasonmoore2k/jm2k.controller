﻿using System;

namespace JM2k.Controller
{
    public interface IPopupService
    {
        TViewController Popup<TViewController>(Func<TViewController, Action> target)
            where TViewController : IPopupViewController;

        TViewController Popup<TViewController, T1>(Func<TViewController, Action<T1>> target, T1 p1)
            where TViewController : IPopupViewController;

        TViewController Popup<TViewController, T1, T2>(Func<TViewController, Action<T1, T2>> target, T1 p1, T2 p2)
            where TViewController : IPopupViewController;

        TViewController Popup<TViewController, T1, T2, T3>(Func<TViewController, Action<T1, T2, T3>> target, T1 p1, T2 p2, T3 p3)
            where TViewController : IPopupViewController;
    }
}