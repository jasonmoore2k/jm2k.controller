﻿namespace JM2k.Controller
{
    public abstract class LightInjectBootstrapper : BootstrapperBase
    {
        protected LightInjectBootstrapper()
            : base(new LightInjectResolver())
        {
        }
    }
}