﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace JM2k.Controller
{
    public class PopupService : IPopupService
    {
        private readonly IResolver _resolver;
        private readonly IViewResolver _viewResolver;

        public PopupService(
            IResolver resolver,
            IViewResolver viewResolver)
        {
            _resolver = resolver;
            _viewResolver = viewResolver;
        }

        public TViewController Popup<TViewController>(Func<TViewController, Action> target)
            where TViewController : IPopupViewController
        {
            var controller = PopupBase<TViewController>();
            target(controller)();
            return controller;
        }

        public TViewController Popup<TViewController, T1>(Func<TViewController, Action<T1>> target, T1 p1)
            where TViewController : IPopupViewController
        {
            var controller = PopupBase<TViewController>();
            target(controller)(p1);
            return controller;
        }

        public TViewController Popup<TViewController, T1, T2>(Func<TViewController, Action<T1, T2>> target, T1 p1, T2 p2)
            where TViewController : IPopupViewController
        {
            var controller = PopupBase<TViewController>();
            target(controller)(p1, p2);
            return controller;
        }

        public TViewController Popup<TViewController, T1, T2, T3>(Func<TViewController, Action<T1, T2, T3>> target, T1 p1, T2 p2, T3 p3)
            where TViewController : IPopupViewController
        {
            var controller = PopupBase<TViewController>();
            target(controller)(p1, p2, p3);
            return controller;
        }

        private TViewController PopupBase<TViewController>()
            where TViewController : IPopupViewController
        {
            var viewController = _resolver.Resolve<TViewController>();

            Type viewType = _viewResolver.Resolve(viewController.ViewModelType);

            Popup(viewType, viewController);

            return viewController;
        }

        private void Popup(Type source, IPopupViewController controller)
        {
            var view = (Control)Activator.CreateInstance(source);
            view.DataContext = controller.ViewModelObject;
            var popup = new ContentDialog
            {
                Title = view.Name,
                Content = view,
                FullSizeDesired = true,
                IsSecondaryButtonEnabled = true,
                SecondaryButtonText = "Close",
            };

            if (controller.Accept != null)
            {
                popup.IsPrimaryButtonEnabled = true;
                popup.PrimaryButtonText = "Accept";
                popup.PrimaryButtonCommand = controller.Accept;
            }

            popup.ShowAsync();
        }
    }
}