﻿using System.Windows.Input;

namespace JM2k.Controller
{
    public interface IPopupViewController : IViewController
    {
        ICommand Accept { get; }
    }
}