﻿using System;

namespace JM2k.Controller
{
    public interface IViewResolver
    {
        Type Resolve<TViewModel>();

        Type Resolve(Type viewModelType);
    }
}