﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace JM2k.Controller
{
    public abstract class BootstrapperBase
    {
        protected readonly IResolver Resolver;

        protected BootstrapperBase(IResolver resolver)
        {
            Resolver = resolver;
            Resolver.RegisterInstance(resolver);
            Resolver.RegisterSingleton<INavigationService, NavigationService>();
            Resolver.RegisterSingleton<IPopupService, PopupService>();
        }

        public TViewController Run<TViewController>(Func<TViewController, Action> target)
            where TViewController : IViewController
        {
            Frame rootFrame = CreateRootFrame();
            var controller = Resolve<INavigationService>().NavigateTo(target);
            Window.Current.Content = rootFrame;
            Window.Current.Activate();
            return controller;
        }

        public TViewController Run<TViewController, T1>(Func<TViewController, Action<T1>> target, T1 p1)
            where TViewController : IViewController
        {
            Frame rootFrame = CreateRootFrame();
            var controller = Resolve<INavigationService>().NavigateTo(target, p1);
            Window.Current.Content = rootFrame;
            Window.Current.Activate();
            return controller;
        }

        public TViewController Run<TViewController, T1, T2>(Func<TViewController, Action<T1, T2>> target, T1 p1, T2 p2)
            where TViewController : IViewController
        {
            Frame rootFrame = CreateRootFrame();
            var controller = Resolve<INavigationService>().NavigateTo(target, p1, p2);
            Window.Current.Content = rootFrame;
            Window.Current.Activate();
            return controller;
        }

        public TViewController Run<TViewController, T1, T2, T3>(Func<TViewController, Action<T1, T2, T3>> target, T1 p1, T2 p2, T3 p3)
            where TViewController : IViewController
        {
            Frame rootFrame = CreateRootFrame();
            var controller = Resolve<INavigationService>().NavigateTo(target, p1, p2, p3);
            Window.Current.Content = rootFrame;
            Window.Current.Activate();
            return controller;
        }

        protected abstract void ConfigureContainer();

        private Frame CreateRootFrame()
        {
            var rootFrame = new Frame();

            Resolver.RegisterInstance(rootFrame);

            ConfigureContainer();

            return rootFrame;
        }

        protected T Resolve<T>()
        {
            return Resolver.Resolve<T>();
        }
    }
}