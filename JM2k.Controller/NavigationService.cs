﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace JM2k.Controller
{
    public class NavigationService : INavigationService
    {
        private readonly Frame _frame;
        private readonly IResolver _resolver;
        private readonly IViewResolver _viewResolver;

        public NavigationService(
            Frame frame,
            IResolver resolver,
            IViewResolver viewResolver)
        {
            _frame = frame;
            _resolver = resolver;
            _viewResolver = viewResolver;

            _frame.Navigated += OnNavigated;
        }

        public void GoBack()
        {
            lock (_frame)
            {
                _frame.GoBack();
            }
        }

        public bool CanGoBack()
        {
            return _frame.CanGoBack;
        }

        public void GoForward()
        {
            _frame.GoForward();
        }

        public bool CanGoForward()
        {
            return _frame.CanGoForward;
        }

        public void GoHome()
        {
            while (CanGoBack())
            {
                GoBack();
            }
        }

        public TViewController NavigateTo<TViewController>(Func<TViewController, Action> target, string region = null)
            where TViewController : IViewController
        {
            var controller = NavigateToBase<TViewController>(region);
            target(controller)();
            return controller;
        }

        public TViewController NavigateTo<TViewController, T1>(Func<TViewController, Action<T1>> target, T1 p1, string region = null)
            where TViewController : IViewController
        {
            var controller = NavigateToBase<TViewController>(region);
            target(controller)(p1);
            return controller;
        }

        public TViewController NavigateTo<TViewController, T1, T2>(Func<TViewController, Action<T1, T2>> target, T1 p1, T2 p2, string region = null)
            where TViewController : IViewController
        {
            var controller = NavigateToBase<TViewController>(region);
            target(controller)(p1, p2);
            return controller;
        }

        public TViewController NavigateTo<TViewController, T1, T2, T3>(Func<TViewController, Action<T1, T2, T3>> target, T1 p1, T2 p2, T3 p3, string region = null)
            where TViewController : IViewController
        {
            var controller = NavigateToBase<TViewController>(region);
            target(controller)(p1, p2, p3);
            return controller;
        }

        private TViewController NavigateToBase<TViewController>(string region)
            where TViewController : IViewController
        {
            var viewController = _resolver.Resolve<TViewController>();

            Type viewType = _viewResolver.Resolve(viewController.ViewModelType);

            var frame = GetFrame(region);
            frame.Navigate(viewType, viewController.ViewModelObject);

            return viewController;
        }

        private Frame GetFrame(string region)
        {
            Frame frame;
            if (region == null)
            {
                return _frame;
            }
            else
            {
                if (!FrameRegister.Frames.TryGetValue(region, out frame))
                {
                    throw new KeyNotFoundException($"Region '{region}' does not exist");
                }
            }

            return frame;
        }

        private void OnNavigated(object sender, NavigationEventArgs e)
        {
            var element = e.Content as FrameworkElement;
            if (element == null)
                return;

            element.DataContext = e.Parameter;
        }
    }
}