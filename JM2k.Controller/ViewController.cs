﻿using System;

namespace JM2k.Controller
{
    public interface IViewController
    {
        Type ViewModelType { get; }

        object ViewModelObject { get; }
    }

    public interface IViewController<out TViewModel> : IViewController
    {
        TViewModel ViewModel { get; }
    }

    public abstract class ViewController : IViewController
    {
        public abstract Type ViewModelType { get; }

        public abstract object ViewModelObject { get; }
    }

    public class ViewController<TViewModel> : ViewController, IViewController<TViewModel>
    {
        public TViewModel ViewModel { get; private set; }

        public override Type ViewModelType => typeof(TViewModel);

        public override object ViewModelObject => ViewModel;

        public ViewController(TViewModel viewModel)
        {
            ViewModel = viewModel;
        }
    }
}