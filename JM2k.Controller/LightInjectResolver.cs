﻿using System;
using JM2k.Controller.LightInject;

namespace JM2k.Controller
{
    public class LightInjectResolver : IResolver
    {
        private readonly ServiceContainer _container;

        public LightInjectResolver()
        {
            _container = new ServiceContainer();
        }

        public T Resolve<T>()
        {
            return _container.GetInstance<T>();
        }

        public object Resolve(Type type)
        {
            return _container.GetInstance(type);
        }

        public void RegisterInstance<T>(T instance) where T : class
        {
            _container.RegisterInstance(instance);
        }

        public void RegisterSingleton<TFrom, TTo>() where TTo : TFrom
        {
            _container.Register<TFrom, TTo>(new PerContainerLifetime());
        }

        public void Register<TFrom, TTo>() where TTo : TFrom
        {
            _container.Register<TFrom, TTo>();
        }
    }
}
