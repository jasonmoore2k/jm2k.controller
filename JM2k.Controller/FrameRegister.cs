using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace JM2k.Controller
{
    public class FrameRegister : DependencyObject
    {
        public static readonly ConcurrentDictionary<string, Frame> Frames = new ConcurrentDictionary<string, Frame>();

        public static readonly DependencyProperty KeyProperty =
            DependencyProperty.RegisterAttached(
                "Key", typeof(string), typeof(FrameRegister),
                new PropertyMetadata(false, OnKeyChanged));

        public static string GetKey(Frame frame)
        {
            return (string)frame.GetValue(KeyProperty);
        }

        public static void SetKey(Frame frame, string value)
        {
            frame.SetValue(KeyProperty, value);
        }

        public static Frame GetFrame(string key)
        {
            Frame frame;
            if (Frames.TryGetValue(key, out frame))
            {
                return frame;
            }

            throw new KeyNotFoundException($"No frame registered with key '${key}'");
        }

        private static void OnKeyChanged(DependencyObject frameObject, DependencyPropertyChangedEventArgs _)
        {
            var frame = (Frame)frameObject;
            var key = GetKey(frame);

            Frames.GetOrAdd(key, k =>
            {
                frame.Unloaded += OnFrameUnloaded;
                return frame;
            });
        }

        private static void OnFrameUnloaded(object sender, RoutedEventArgs e)
        {
            var frame = (Frame)sender;
            var key = GetKey(frame);
            Frame existing;
            if (Frames.TryRemove(key, out existing) &&
                frame != existing)
            {
                throw new InvalidOperationException($"Frame with key ${key} removed, but was a different instance");
            }
        }
    }
}